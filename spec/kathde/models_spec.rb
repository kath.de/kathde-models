# frozen_string_literal: true

require 'rails_helper'

describe Kathde::Models do
  it 'has a version number' do
    expect(Kathde::Models::VERSION).not_to be nil
  end
end
