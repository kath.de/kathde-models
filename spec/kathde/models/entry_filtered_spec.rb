# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Entry, type: :model do
  describe '.by_slug' do
    let!(:slugged_entry) { create(:entry) }

    before { create_list(:entry, 2) }

    it 'selects entries with slug' do
      expect(Entry.by_slug(slugged_entry.slug).first).to match(slugged_entry)
    end
  end

  describe '.by_author' do
    let!(:authored_entry) { create(:entry, author: author) }
    let!(:unauthored_entry) { create(:entry) }
    let!(:author) { create(:author, slug: 'philipp-mueller', display_name: 'Philipp Müller') }

    it 'selects entries by author display_name' do
      expect(Entry.by_author('Philipp Müller')).to match_array(authored_entry)
    end

    it 'selects entries by author id' do
      expect(Entry.by_author(author.id)).to match_array(authored_entry)
    end

    it 'selects entries by multiple author names' do
      expect(Entry.by_author(['philipp-mueller', 'Nobody'])).to match_array(authored_entry)
    end

    it 'does not select entries by partial author name' do
      expect(Entry.by_author('philipp')).to match_array(authored_entry)
    end

    it 'does select entries by fuzzy partial author name' do
      expect(Entry.by_author('%philipp%')).to match_array(authored_entry)
    end

    it 'does not select entries for unknown author' do
      expect(Entry.by_author(['Nobody'])).to be_empty
    end
  end

  describe '.by_any' do
    let!(:petrus)  { create(:entry,   :published, slug: 'petrus') }
    let!(:paulus)  { create(:entry,   :published, slug: 'paulus') }
    let!(:jakobus) { create(:entry, :unpublished, slug: 'jakobus') }
    let(:all) { [petrus, paulus, jakobus] }

    context 'empty terms' do
      it { expect(Entry.by_any('')).to match_array(all) }

      it { expect(Entry.by_any([nil, ''])).to match_array(all) }

      it { expect(Entry.by_any([nil, ''], set_operator: :or)).to match_array(all) }
    end

    context 'using single slug' do
      it { expect(Entry.by_any('petrus')).to match_array(petrus) }
    end

    context 'using multiple slugs' do
      context 'with conjunction' do
        it { expect(Entry.by_any(%w[petrus paulus], set_operator: :and)).to be_empty }

        it { expect(Entry.by_any(%w[petrus paulus notaslug], set_operator: :conjunction)).to be_empty }

        it { expect(Entry.by_any(%w[p us], set_operator: :and)).to match_array([petrus, paulus]) }

        it { expect(Entry.by_any(%w[notaslug notanotherslug], set_operator: :conjunction)).to be_empty }
      end

      context 'with disjunction' do
        it { expect(Entry.by_any(%w[petrus paulus], set_operator: :disjunction)).to match_array([petrus, paulus]) }

        it { expect(Entry.by_any(%w[petrus paulus notaslug], set_operator: :or)).to match_array([petrus, paulus]) }

        it { expect(Entry.by_any(%w[p us], set_operator: :disjunction)).to match_array(all) }

        it { expect(Entry.by_any(%w[notaslug notanotherslug], set_operator: :disjunction)).to be_empty }
      end

      context 'with invalid set_operator' do
        it { expect { Entry.by_any(['petrus'], set_operator: :whatever) }.to raise_error ArgumentError }
      end
    end
  end
end
