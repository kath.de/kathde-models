# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Entry, type: :model do
  describe '#published_at' do
    it 'is unpublished by default' do
      expect(Entry.new).not_to be_published
    end

    it 'is valid when published_at is set but state is draft' do
      expect(Entry.new(published_at: Time.zone.now, status: :draft)).to be_valid
    end
  end

  describe '#publish' do
    it 'updates accordingly' do
      entry = build(:entry, :unpublished)
      expect { entry.publish }.to change { entry.published? }.from(false).to(true)
    end

    it 'is not republished' do
      entry = build(:entry, :published)
      expect { entry.publish }.not_to(change { entry.published_at })
    end
  end

  describe '#unpublish' do
    it 'updates accordingly' do
      entry = build(:entry, :published)
      expect { entry.unpublish }.to change { entry.published? }.from(true).to(false)
    end
  end

  describe '#slug' do
    let(:dirty_slug) { 'Gummibärenbande im Kölner Dom' }
    let(:test_slug) { 'gummibaerenbande-im-koelner-dom' }

    context 'with invalid slug' do
      let(:entry) { create(:entry).tap { |entry| entry.assign_attributes(slug: dirty_slug) } }

      it 'is invalid' do
        expect(entry).not_to be_valid
      end
    end

    context 'with valid slug' do
      let(:entry) { create(:entry).tap { |entry| entry.assign_attributes(slug: test_slug) } }

      it 'is valid' do
        expect(entry).to be_valid
      end
    end
  end

  describe '#slug' do
    context 'valid slug' do
      let(:slug) { 'abcdefghijklmnopqrstuvwxyz-_0123456789' }

      it 'is considered valid' do
        expect(Entry.new(type: Entry, slug: slug)).to be_valid
      end
    end

    context 'invalid slug' do
      let(:slug) { 'abcdefghijklmnopqrstuvwxyz-_0123456789%' }

      it 'is considered invalid' do
        expect(Entry.new(type: Entry, slug: slug)).not_to be_valid
      end
    end

    context 'slug with path' do
      let(:slug) { 'test/path/multi' }

      context 'in default entry class' do
        it 'is considered invalid' do
          expect(Entry.new(type: Entry, slug: slug)).not_to be_valid
        end
      end

      context 'in entry class with slug path' do
        it 'is considered valid' do
          expect(EntryWithSlugPath.new(type: EntryWithSlugPath, slug: slug)).to be_valid
        end
      end
    end
  end
end

class EntryWithSlugPath < Entry
  def allow_path_in_slug?
    true
  end
end
