# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Page, type: :model do
  describe '#slug' do
    context 'invalid slug' do
      let(:slug) { 'abcdefghijklmnopqrstuvwxyz-_0123456789%' }

      it 'is considered invalid' do
        expect(Page.new(slug: slug)).not_to be_valid
      end
    end

    context 'slug with path' do
      let(:slug) { 'test/path/multi' }

      it 'is considered valid' do
        expect(Page.new(slug: slug)).to be_valid
      end
    end
  end
end
