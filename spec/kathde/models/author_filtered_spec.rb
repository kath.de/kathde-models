# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Author, type: :model do
  let!(:author) do
    create(:author, slug: 'philipp-mueller', display_name: 'Philipp Müller')
  end

  before do
    _other_authors = create_list(:author, 3)
  end

  describe '.by_term' do
    it 'selects entry by author display_name' do
      expect(Author.by_term('Philipp Müller')).to match_array(author)
    end

    it 'selects entry by author name' do
      expect(Author.by_term(['philipp-mueller', 'unnkown-nobody'])).to match_array(author)
    end

    it 'does not select entry for unknown author' do
      expect(Author.by_term(['Nobody'])).to be_empty
    end
  end
end
