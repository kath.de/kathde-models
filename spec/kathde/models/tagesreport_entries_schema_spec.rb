# frozen_string_literal: true

require 'kathde/models/tagesreport'
require 'rails_helper'

RSpec::Matchers.define :match_schema do |_schema, strict = false|
  match do |json|
    schema_path = Tagesreport::PROFILE_JSON_SCHEMA

    JSON::Validator.validate!(schema_path, json, strict: strict)
  end
end

# rubocop:disable RSpec/DescribeClass
RSpec.describe 'tagesreport-entries.json-schema', type: :schema do
  let(:schema) { 'tagesreport-entries' }
  let(:simple_entries) do
    {
      'Themen des Tages' => [
        {
          'url' => 'http://de.radiovaticana.va/news/2016/10/30/der_papst_und_die_reformation_%E2%80%9Ewir_m%C3%BCssen_gemeinsam_beten%E2%80%9C/1267631',
          'titel' => 'Der Papst und die Reformation: „Wir müssen gemeinsam beten“',
          'untertitel' => nil,
          'tags' => 'Papst, Reformation, Gebet',
        },
      ],
      'Service und Spiritualität' => [
        {
          'url' => 'http://www.kath.de/Kirchenjahr/allerheiligen_1_november.php',
          'titel' => 'Allerheiligen',
          'quelle' => "Aus unserem Lexikon \"Kirchenjahr \u0026 Brauchtum\"",
        },
      ],
      'Bistümer und Werke' => [
        {
          'url' => 'http://www.bonifatiuswerk.de/werk/aktuelles/newsausgabe/article/der-papst-ist-in-schweden-angekommen/',
          'titel' => 'Der Papst ist in Schweden angekommen',
          'untertitel' => 'Ökumenische Gedenkveranstaltung zum 500. Jahrestag der Reformation',
          'tags' => %w[Papst Ökumene Reformation Reformationsjubiläum Schweden],
        }, {
          'url' => 'http://www.drs.de/service/nachrichten/a-unterwegs-mit-sankt-martin-00006019.html',
          'titel' => 'Unterwegs mit Sankt Martin',
          'untertitel' => "Kirchenmagazin Alpha \u0026 Omega zum Martinusweg",
          'tags' => 'Sankt Martin, Martinusweg',
        }, {
          'url' => 'http://erzbistum-bamberg.de/nachrichten/hoffnung-im-november/013851bb-a87f-4bbd-b328-15897c2adc55?mode=detail',
          'titel' => 'Hoffnung im November',
          'untertitel' => 'Was die katholische Kirche an Allerheiligen feiert.',
          'tags' => 'Allerheiligen',
        }, {
          'url' => 'http://www.erzbistumberlin.de/medien/pressestelle/aktuelle-pressemeldungen/pressemeldung/datum/2016/10/31/dritte-bernhard-lichtenberg-wallfahrt/',
          'titel' => 'Dritte Bernhard-Lichtenberg-Wallfahrt',
          'datum' => '2016-03-05',
          'untertitel' => 'Gedenktag des seligen Bernhard Lichtenberg',
          'tags' => 'Wallfahrt, Bernhard Lichtenberg, Deutsche Heilige',
        }, {
          'url' => 'https://www.bistumlimburg.de/meldungen/meldung-detail/meldung/kaffeeklatsch-als-kontaktboerse.html',
          'titel' => 'Kaffeeklatsch als Kontaktbörse',
          'untertitel' => 'Im cafe deutschland sind Geflüchtete willkommen',
          'tags' => 'Flüchtlinge, Caritas',
        }, {
          'url' => 'https://www.bistum-hildesheim.de/bistum/nachrichten/artikel/news-title/basilika-st-cyriakus-in-duderstadt-wurde-renoviert-11836/',
          'titel' => 'Basilika St. Cyriakus in Duderstadt wurde renoviert',
          'untertitel' => 'Am Samstag, 5. November, öffnet die Kirche für eine Dankandacht um 17 Uhr erstmals wieder ihre Pforten',
          'tags' => 'Kirchenrenovierung',
        },
      ],
      'Pressespiegel' => nil,
    }
  end
  let(:nested_entries) do
    {
      'Themen des Tages' => [
        {
          'url' =>  'http://de.radiovaticana.va/news/2016/10/30/der_papst_und_die_reformation_%E2%80%9Ewir_m%C3%BCssen_gemeinsam_beten%E2%80%9C/1267631',
          'titel' => 'Der Papst und die Reformation: „Wir müssen gemeinsam beten“',
          'untertitel' => nil,
          'tags' => 'Papst, Reformation, Gebet',
          'außerdem' => [
            {
              'url' => 'http://www.bonifatiuswerk.de/werk/aktuelles/newsausgabe/article/der-papst-ist-in-schweden-angekommen/',
              'titel' => 'Der Papst ist in Schweden angekommen',
              'untertitel' => 'Ökumenische Gedenkveranstaltung zum 500. Jahrestag der Reformation',
              'tags' => %w[Papst Ökumene Reformation Reformationsjubiläum Schweden],
            },
          ],
        },
      ],
    }
  end

  it 'simple entries are valid on schema' do
    expect(simple_entries).to match_schema(schema)
  end

  it 'nested entries are valid on schema' do
    expect(nested_entries).to match_schema(schema)
  end
end
