# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Tagesreport::ReportEntry, type: :model do
  describe "#date" do
    it "allows Time value" do
      time = Time.zone.now
      entry = Tagesreport::ReportEntry.new({"datum" => time})
      expect(entry.date).to eq(time)
    end

    it "allows Date value" do
      date = Date.new
      entry = Tagesreport::ReportEntry.new({"datum" => date})
      expect(entry.date).to eq(date)
    end

    it "falls back to tagesreport when nil" do
      time = Date.new
      entry = Tagesreport::ReportEntry.new({"datum" => nil}, tagesreport: Tagesreport.new(date: time))
      expect(entry.date).to eq(time)
    end

    it "is empty" do
      entry = Tagesreport::ReportEntry.new({"datum" => ''}, tagesreport: Tagesreport.new(date: Time.zone.now))
      expect(entry.date).to be false
    end

    it "is false" do
      entry = Tagesreport::ReportEntry.new({"datum" => false}, tagesreport: Tagesreport.new(date: Time.zone.now))
      expect(entry.date).to be false
    end
  end

  describe "#source" do
    it "uses publisher" do
      publisher = build(:publisher)
      entry = Tagesreport::ReportEntry.new({"quelle" => nil}, publisher: publisher)
      expect(entry.source).to eq(publisher.title)
    end

    it "allows override" do
      publisher = build(:publisher)
      entry = Tagesreport::ReportEntry.new({"quelle" => "Override"}, publisher: publisher)
      expect(entry.source).to eq("Override")
    end

    it "is empty" do
      publisher = build(:publisher)
      entry = Tagesreport::ReportEntry.new({"quelle" => ''}, publisher: publisher)
      expect(entry.source).to be false
    end

    it "is false" do
      publisher = build(:publisher)
      entry = Tagesreport::ReportEntry.new({"quelle" => false}, publisher: publisher)
      expect(entry.source).to be false
    end
  end

  describe ".collect_urls" do
    it "collects urls" do
      collected = Tagesreport::ReportEntry.collect_urls([
        { "url" => "foo.net" },
        { "url" => "bar.net" },
      ])
      expect(collected).to eq(["foo.net", "bar.net"])
    end

    it "collects nested urls" do
      collected = Tagesreport::ReportEntry.collect_urls([
        { "url" => "foo.net" },
        {
          "url" => "baz.net/bar",
          "außerdem" => [
            { "url" => "baz.net" },
            { "url" => "baz.net/bar" }
          ]
        },
        { "url" => "bar.net" },
      ])
      expect(collected).to eq(["foo.net", "baz.net/bar", "baz.net", "baz.net/bar", "bar.net"])
    end
  end
end
