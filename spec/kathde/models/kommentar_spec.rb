# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Kommentar, type: :model do
  let(:test_title) { 'Gummibärenbande im Kölner Dom' }
  let(:test_slug) { 'gummibaerenbande-im-koelner-dom' }
  let(:other_slug) { 'other-slug' }

  describe '#title' do
    context 'when attribute is set' do
      let(:kommentar) do
        Kommentar.new.tap do |k|
          k.title = test_title
        end
      end

      it 'stores value as payload' do
        expect(kommentar.payload).to include('title' => test_title)
      end

      it 'has value accessible as attribute' do
        expect(kommentar.title).to eq(test_title)
      end
    end

    context 'when initializing' do
      let(:kommentar) { Kommentar.new(title: test_title) }

      it 'stores value as payload' do
        expect(kommentar.payload).to include('title' => test_title)
      end

      it 'has value accesible as attribute' do
        expect(kommentar.title).to eq(test_title)
      end
    end
  end

  describe '.published' do
    let(:published_entries) { create_list(:kommentar, 3, :published) }
    let(:unpublished_entries) { create_list(:kommentar, 3, :unpublished) }

    it 'selects published entries' do
      expect(Kommentar.published).to match_array published_entries
    end

    it 'selects unpublished entries' do
      expect(Kommentar.unpublished).to match_array unpublished_entries
    end

    it 'follows change' do
      k = create(:kommentar, :unpublished)
      k.publish!
      expect(Kommentar.published).to eq [k]
    end
  end

  describe '#cover' do
    it 'associates cover' do
      #ActiveRecord::Base.logger = Logger.new(STDOUT) if defined?(ActiveRecord::Base)
      image = Image.new
      image.image.attach(create_file_blob('image.jpg'))
      image.save

      kommentar = Kommentar.new(title: "foo", cover: image)
      kommentar.save
      expect(kommentar.cover.image).to eq image.image
    end
  end
end
