require 'rails_helper'

RSpec.describe Wochenrueckblick, type: :model do
  describe '.recent' do
    it 'orders strictly by publication date' do
      newest = create(:wochenrueckblick, :published, published_at: Time.new(2018, 07, 28))
      oldest = create(:wochenrueckblick, :published, published_at: Time.new(2018, 07, 21))

      expect(Wochenrueckblick.recent).to eq([newest, oldest])
    end
  end
end
