# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Image, type: :model do
  let(:image) { create(:image) }

  it '.new' do
    image = Image.new(description: 'foo bar baz', cite: 'Eva Plast 2019')
    expect(image.description).to eq 'foo bar baz'
    expect(image.cite).to eq 'Eva Plast 2019'
  end

  it 'attach image file' do
    image = Image.new
    image.image.attach(create_file_blob('image.jpg'))
    expect(image.image.attached?).to eq(true)
    expect(image.generate_slug).to eq('image.jpg')
  end
end
