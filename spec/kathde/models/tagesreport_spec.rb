# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Tagesreport, type: :model do
  it 'has JsonValidator' do
    expect(defined?(JsonValidator)).to be_truthy
  end

  describe '#aufmacher' do
    let(:aufmacher_body) { 'Papst Franziskus hat 7 Heiligsprechungen vorgenommen. In der Messe am Sonntag auf dem Petersdom sprach er sechs Männer und eine Frau heilig. Dabei betonte er das Gebet. Nie aufzuhören zu Gott zu schreien, sei was Heilige ausmache. Beten bedeute nicht in eine Traumwelt oder sein eigenes Ego zu fliehen. Sondern dein Heiligen Geist in sich beten zu lassen. Etwa 80.000 Gläubige waren zum Gottesdienst gekommen.' }
    let(:aufmacher_title) { 'Vom jungen Märtyrer bis einfachen Priester' }
    let(:tagesreport) { build(:tagesreport, aufmacher_body: aufmacher_body, aufmacher_title: aufmacher_title) }

    it 'sets aufmacher_body' do
      expect(tagesreport.aufmacher_body).to eq(aufmacher_body)
    end

    it 'sets aufmacher_title' do
      expect(tagesreport.aufmacher_title).to eq(aufmacher_title)
    end
  end

  describe '#entries' do
    let(:entries) do
      {
        'Pressespiegel' => [{
          url: 'http://www.dw.com/de/heiligsprechung-auf-dem-petersplatz-papst-feiert-neue-heilige/a-36054181',
          title: 'Papst feiert neue Heilige',
          subtitle: 'Heiligsprechung auf dem Petersplatz',
          date: '2016-10-16',
          source: 'Deutsche Welle',
          related: [{
            link: 'http://video.tagesspiegel.de/5172556555001',
            title: 'Argentinier schwören auf „Gaucho-Priester“',
            source: 'Der Tagesspiegel',
            date: '2016-10-16',
          },],
        }, {
          url: 'https://www.welt.de/newsticker/news2/article158803713/Gericht-in-Argentinien-verurteilt-katholische-Kirche-erstmals-wegen-Missbrauchs.html',
          title: 'Gerichtverurteilt katholische Kirche',
          subtitle: 'Erstmals in Argentinien wegen Missbrauchs',
          date: '2016-10-16',
        },],
        'Bistümer und Werke' => [{
          url: 'http://www.erzbistum-freiburg.de/html/aktuell/aktuell_aktuell_u.html?t=8a46a9ba1022efb5e44f112492c7d218&tto=6885cdb4&&&&m=19781&cataktuell=&artikel=66727&stichwort_aktuell=&default=true',
          title: 'Miteinander und füreinander Beten',
          subtitle: 'Kirchen als lebendige Orte des Gebetess',
          date: '2016-10-16',
        },],
        'Themen des Tages' => [],
      }
    end

    let(:tagesreport) { Tagesreport.new(entries_hash: entries) }

    it 'gets set' do
      expect(tagesreport.entries_hash).to eq(entries.deep_stringify_keys)
    end

    it 'correctly identifies available sections' do
      expect(tagesreport.section_titles).to contain_exactly('Pressespiegel', 'Bistümer und Werke')
    end

    let(:entries_invalid_yaml) { <<-EOF }
      "Themen des Tages":
      - title: """Foobar"""
      EOF

    it "handles invalid yaml" do
      tagesreport = Tagesreport.new(entries: entries_invalid_yaml)
      expect(tagesreport.entries_hash).to eq({})
      expect(tagesreport.errors.details[:entries]).to include({error: "(<unknown>): did not find expected key while parsing a block mapping at line 2 column 9"})
    end

    it "handles missing information" do
      tagesreport = Tagesreport.new(entries_hash: {
        'Themen des Tages': [{
          'title': 'Foobar'
        }]})
      tagesreport.sections
    end
  end

  describe 'entries_yaml=' do
    let(:entries_yaml) do
      '---
Themen des Tages:
- titel: "Fastenbotschaft: „Der andere ist Geschenk“"
  url: http://de.radiovaticana.va/news/2017/02/07/fastenbotschaft_%E2%80%9Eder_andere_ist_geschenk%E2%80%9C/1290872
  datum: 2017-02-07
Bistümer und Werke:
- titel: "„Jesus cries“ - Die letzten Tage im Leben Jesu"
  untertitel: Filmgespräch mit Regisseurin Brigitte Maria Mayer in Geisenheim
  url: https://www.bistumlimburg.de/meldungen/meldung-detail/meldung/jesus-cries-die-letzten-tage-im-leben-jesu.html
  Pressespiegel:'
    end
    let(:tagesreport) { Tagesreport.new(entries: entries_yaml) }

    it 'parses entries' do
      expect(tagesreport.entries_list.length).to match(2)
    end

    it 'parses sections' do
      expect(tagesreport.section_titles).to contain_exactly('Themen des Tages', 'Bistümer und Werke')
    end

    it "parses 'außerdem'" do
      tagesreport = Tagesreport.new(entries: <<-YAML)
        Themen des Tages:
        - titel: foo
          url: https://example.com
          außerdem:
          - titel: bar
            url: https://example.org
        YAML

      entry = Tagesreport::ReportEntry.new(section: "Themen des Tages", tagesreport: tagesreport)
      entry.titel = "bar"
      entry.url = "https://example.org"
      expect(tagesreport.sections["Themen des Tages"].first.related).to eq([
        entry
      ])
    end
  end

  describe "sections" do
    it "sets publisher for related entries" do
      tagesreport = Tagesreport.new(entries: <<-YAML)
          Themen des Tages:
          - titel: foo
            url: https://example.com
            außerdem:
            - titel: known_url
              url: https://example.org
            - titel: unknown_url
              url: https://example.net
            - title: url_with_path
              url: https://example.org/foo
            - title: explicit_publisher
              url: https://example.net
              quelle: Example.NET
          YAML

      publishers = {
        "example.com" => build(:publisher, title: 'Site A', slug: 'example.com'),
        "example.org" => build(:publisher, title: 'Site B', slug: 'example.org'),
        "example.org/foo" => build(:publisher, title: 'Site B/Foo', slug: 'example.org/foo')
      }
      sections = tagesreport.sections(publishers)
      entry = sections["Themen des Tages"].first
      expect(entry.publisher).to eq publishers["example.com"]
      expect(entry.related[0].publisher).to eq publishers["example.org"]
      expect(entry.related[1].publisher.title).to eq 'Example.net'
      expect(entry.related[1].publisher.new_record?).to be true
      expect(entry.related[2].publisher).to eq publishers["example.org/foo"]
      expect(entry.related[3].source).to eq "Example.NET"
      expect(entry.related[3].publisher).to eq Publisher.new(title: "Example.net", slug: 'example.net')
    end

    it "loads empty section" do
      tagesreport = Tagesreport.new(entries: <<-YAML)
        "Foo":
        - title: foo
          url: https://example.com
        "Bar":
        YAML

      expect(tagesreport.sections["Bar"]).to be_empty
    end
  end
end
