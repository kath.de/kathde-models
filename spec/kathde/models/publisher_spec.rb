# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Publisher, type: :model do
  describe '#validate' do
    it 'is invalid without any values' do
      expect(Publisher.new).not_to be_valid
    end

    it 'is valid with values' do
      expect(Publisher.new(domain: 'kath.de', title: 'Kath.de')).to be_valid
    end
  end

  describe '.recently_created' do
    let!(:older_publishers) { create_list(:publisher, 3, created_at: Time.zone.yesterday) }

    it('is empty by default') { expect(described_class.recently_created).to be_empty }

    context 'some publishers exist' do
      let!(:todays_publishers) { create_list(:publisher, 2, created_at: Time.zone.now) }

      it('finds recently created') do
        expect(described_class.recently_created).to match_array(todays_publishers)
      end
    end
  end

  describe '.normalize_url' do
    it { expect(described_class.normalize_url('https://www.example.com/with?path=present')).to eq('example.com/with') }

    it { expect(described_class.normalize_url('http://de.radiovaticana.va')).to eq('de.radiovaticana.va') }

    it { expect(described_class.normalize_url('de.radiovaticana.va')).to eq('de.radiovaticana.va') }

    it { expect(described_class.normalize_url('www.Example.com')).to eq('example.com') }

    it { expect(described_class.normalize_url('is no url')).to eq('is no url') }
  end

  describe '.by_url' do
    describe 'loads publisher' do
      it 'with same url' do
        publisher = create(:publisher, domain: 'example.com')
        create(:publisher)

        expect(Publisher.by_url('example.com')).to eq(publisher)
      end

      it 'with url subpath' do
        publisher = create(:publisher, domain: 'example.com')
        create(:publisher)

        expect(Publisher.by_url('example.com/foo')).to eq(publisher)
      end

      it 'with no matching url' do
        create(:publisher, domain: 'example.com')

        expect(Publisher.by_url('http://does-not-exist.com')).to be_nil
      end

      context 'sub-url' do
        it 'with same url' do
          publisher = create(:publisher, domain: 'example.com/foo')
          create(:publisher, domain: 'example.com/foo2')
          create(:publisher, domain: 'example.com')

          expect(Publisher.by_url('example.com/foo')).to eq(publisher)
        end

        it 'with url subpath' do
          publisher = create(:publisher, domain: 'example.com/foo')
          create(:publisher, domain: 'example.com/foo2')
          create(:publisher, domain: 'example.com')

          expect(Publisher.by_url('example.com/foo/bar')).to eq(publisher)
        end
      end
    end
    it "array arguments" do
      example_foo = create(:publisher, domain: 'example.com/foo')
      example_foo2 = create(:publisher, domain: 'example.com/foo2')
      example = create(:publisher, domain: 'example.com')

      publishers = {
        "example.com/bar" => example,
        "example.com" => example,
        "example.com/fooo" => example_foo,
        "example.com/foo2/bar" => example_foo2,
      }

      results = Publisher.by_url(publishers.keys)
      publishers.each do |key, value|
        expect(results[key]).to eq(value)
      end
    end
  end
end
