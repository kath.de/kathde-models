# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Author, type: :model do
  let(:author) { create(:author) }

  describe '#entries' do
    let!(:authored_articles) { create_list(:kommentar, 5, :published, author: author) }
    let!(:unauthored_article) { create(:kommentar, :published) }

    it 'includes entries authored by author' do
      expect(author.entries).to match_array(authored_articles)
    end

    it 'does not include other entries' do
      expect(author.entries).not_to include(unauthored_article)
    end
  end
end
