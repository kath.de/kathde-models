# frozen_string_literal: true

# RSpec
# spec/support/factory_bot.rb
RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods

  Dir.glob(File.join(File.dirname(__FILE__), '..', 'factories', '*.rb'), &method(:require))
end
