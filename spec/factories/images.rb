# frozen_string_literal: true

FactoryBot.define do
  factory :image do
    sequence(:copyright) do |n|
      f = %w[Hans Peter Klaus Thomas Jochen Michael Matthias Peter Paul Stefan Kilian]
      l = %w[Müller Maier Schmidt Huber Bauer Henkel Rose Wagner Schneider Hirsch]
      "#{f[n % f.size]} #{l[n % l.size]}"
    end

    description { 'Foobar Image' }

    created_at { '2016-10-08 12:50:34' }
    updated_at { '2016-10-08 12:50:34' }

    image { Kathde::Models::ActiveStorageHelpers.create_file_blob }
  end
end
