# frozen_string_literal: true

FactoryBot.define do
  factory :publisher do
    sequence(:domain) do |n|
      d = %w[welt.de bistum-goerlitz.de domradio.de radiovaticana.va hinsehen.net]
      d[n % d.size]
    end

    sequence(:title) do |n|
      d = ['Die WELT', 'Bistum Görlitz', 'Domradio', 'Radio Vatikan', 'Hinsehen.net']
      d[n % d.size]
    end
  end
end
