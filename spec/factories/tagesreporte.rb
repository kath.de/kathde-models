# frozen_string_literal: true

FactoryBot.define do
  factory :tagesreport do
    sequence(:aufmacher_title) do |n|
      a = ['Gummibärenbande im Kölner Dom',
           'Gänseblümchen für Gänswein: Papst Benedikt gratuliert im Vatikanischen Garten',
           'Habemus Papam: Halbwaisen freuen sich über Hochzeit ihrer Mutter',
           'Hello World',
           'Lorem ipsum',
           'Hokus Pokus',
           'Railing on Rubies',]
      a[n % a.size]
    end

    aufmacher_body { 'Auf Einladung von Kardinal Woelki hüpfte die Gummibärenbande durch den Kölner Dom.' }
    created_at { '2016-10-08 12:50:34' }
    updated_at { '2016-10-08 12:50:34' }
    date { '2016-10-17' }

    slug do
      date
    end

    trait :unpublished do
      published_at { nil }
      status { 'draft' }
    end

    association :author, factory: :author

    trait :published do
      published_at { '2016-10-08 12:50:34' }
      status { 'published' }
    end
  end
end
