# frozen_string_literal: true

FactoryBot.define do
  factory :wochenrueckblick do
    sequence(:title) do |n|
      "#{n}. bis #{n+1}. Juni 2018"
    end

    body { 'In der Woche ist vieles geschehen.' }
    teaser_text { 'Alles wichtige in der Woche' }
    sequence(:created_at) { |n| '2018-06-%02d 12:50:34' % n }
    sequence(:updated_at) { |n| '2018-06-%02d 12:50:34' % n }

    slug do
      created_at[0..9]
    end

    trait :unpublished do
      published_at { nil }
    end

    association :author, factory: :author

    trait :published do
      sequence(:published_at) { |n| '2018-06-%02d 18:50:34' % n }
      status { 'published' }
    end
  end
end
