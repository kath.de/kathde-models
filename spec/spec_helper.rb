# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'activerecord_json_validator'
require "active_storage/attached"
require "active_storage/reflection"

ActiveRecord::Base.include ActiveStorage::Attached::Model
ActiveRecord::Base.include ActiveStorage::Reflection::ActiveRecordExtensions
ActiveRecord::Reflection.singleton_class.prepend(ActiveStorage::Reflection::ReflectionExtension)

require 'kathde/models'
require 'database_cleaner'
require 'factory_bot'
require 'active_support'
require 'active_support/core_ext/time/zones'
require 'yaml'

RSpec.configure do |config|
  # config.disable_monkey_patching!
  config.default_formatter = 'doc' if config.files_to_run.one?
  config.profile_examples = 0
  Kernel.srand config.seed

  Time.zone = ActiveSupport::TimeZone.find_tzinfo 'Europe/Berlin'

  config.before :suite do
    dbconfig = ENV['DATABASE_URL']
    dbconfig ||= YAML.safe_load(File.open('db/config.yml'), [], [], true)['test']
    ActiveRecord::Base.establish_connection(dbconfig)
  end
end

require 'support/factory_bot'
require 'support/database_cleaner'
