# frozen_string_literal: true

class CreateEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :entries do |t|
      t.string :slug, index: true
      t.string :type, index: true
      t.jsonb :payload
      t.datetime :published_at, index: true
      t.integer :author_ids, array: true, default: []
      t.string :status, default: 'draft', index: true

      t.timestamps
    end

    add_index :entries, :author_ids, using: 'gin'
    add_index :entries, :payload, using: 'gin'
  end
end
