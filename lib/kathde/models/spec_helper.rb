module Kathde::Models::ActiveStorageHelpers
  extend self
  
  # ported from https://github.com/rails/rails/blob/4a17b26c6850dd0892dc0b58a6a3f1cce3169593/activestorage/test/test_helper.rb#L52
  def create_file_blob(filename = "image.jpg", content_type = "image/jpeg", metadata = nil)
    ActiveStorage::Blob.create_after_upload! io: model_file_fixture_path(filename).open, filename: filename, content_type: content_type, metadata: metadata
  end

  def model_file_fixture_path(fixture_name)
    path = Pathname.new(File.join(model_file_fixtures_path, fixture_name))

    if path.exist?
      path
    else
      msg = "the directory '%s' does not contain a file named '%s'"
      raise ArgumentError, msg % [model_file_fixtures_path, fixture_name]
    end
  end

  attr_writer :model_file_fixture_path

  def model_file_fixtures_path
    @model_file_fixtures_path ||= Pathname.new(File.expand_path('../../../../spec/fixtures/files', __FILE__))
  end
end
