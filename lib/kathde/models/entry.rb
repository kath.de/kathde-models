# frozen_string_literal: true

require 'active_record'
require 'jsonb_accessor'
require 'kathde/models/entry/payload'
require 'kathde/models/concerns/authored'
require 'kathde/models/concerns/covered'
require 'kathde/models/concerns/preview_token'
require 'kathde/models/concerns/publishable'
require 'kathde/models/concerns/searchable'
require 'kathde/models/concerns/slugged'


class Entry < ActiveRecord::Base
  include Payload
  include Kathde::Models::Concerns::Slugged
  include Kathde::Models::Concerns::Authored
  include Kathde::Models::Concerns::Searchable
  include Kathde::Models::Concerns::Publishable
end
