# frozen_string_literal: true

require 'active_storage_validations'

class Image < ActiveRecord::Base
  include Kathde::Models::Concerns::Slugged
  include Kathde::Models::Concerns::Searchable

  belongs_to :authors

  has_many :entries, foreign_key: "cover_id"

  validates :image, attached: true, content_type: ["image/png", "image/jpeg", "image/jpg"]
  validates :cite, presence: true

  def allow_path_in_slug?
    true
  end

  def generate_slug
    if image.attached?
      image.filename.to_s.downcase
    end
  end
end

# FIXME: For some reason has_one_attached raises "No connection pool with 'primary' found" when called inline
ActiveSupport.on_load(:action_controller) do
  Image.has_one_attached :image
end
