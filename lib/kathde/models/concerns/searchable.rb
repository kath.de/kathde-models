# frozen_string_literal: true

module Kathde::Models::Concerns
  module Searchable
    extend ActiveSupport::Concern

    included do
      def self.by_any(terms, search_fields = nil, set_operator: :conjunction)
        terms = Array(terms)
        search_fields ||= any_search_fields

        term_queries = build_term_queries(terms, search_fields)

        case set_operator
        when :conjunction, :and
          term_queries.inject(self, :where)
        when :disjunction, :or
          base, *others = term_queries
          where(others.inject(base, :or))
        else
          raise ArgumentError, "Unknown set_operator :#{set_operator}", caller
        end
      end

      scope :by_id, ->(ids) { where(id: ids) }
    end

    class_methods do
      def any_search_fields
        [:slug]
      end

      def arel_column_node(attribute)
        return nil if attribute.nil?

        if respond_to?(:payload_attribute?) && payload_attribute?(attribute)
          Arel::Nodes::InfixOperation.new('->>', payload_column, Arel::Nodes::Quoted.new(attribute))
        else
          arel_table[attribute.to_sym]
        end
      end

      private

      def build_term_queries(terms, search_fields)
        terms.map do |term|
          term = "%#{term}%"
          base, *others = search_fields.map do |field|
            col = arel_column_node(field)
            if term == '%*%'
              col.not_eq(nil)
            else
              col.matches(term)
            end
          end
          others.inject(base, :or)
        end
      end
    end
  end
end
