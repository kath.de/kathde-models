# frozen_string_literal: true

module Kathde::Models::Concerns
  module Covered
    extend ActiveSupport::Concern

    included do
      belongs_to :cover, class_name: 'Image'
    end
  end
end
