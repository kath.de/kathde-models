# frozen_string_literal: true

module Kathde::Models::Concerns
  module Slugged
    extend ActiveSupport::Concern

    included do
      validates :slug,
                length: { maximum: 255 }#,
                #uniqueness: { scope: :type }

      validates :slug, presence: true, if: :slug_presence_required?
      validates :slug, format: { with: /\A[a-z0-9\-_\. ]*\z/ }, unless: :allow_path_in_slug?
      validates :slug, format: { with: %r{\A[a-z0-9\-_\./]*\z} }, if: :allow_path_in_slug?

      scope :by_slug, ->(slug) { where('slug LIKE ?', slug) }
    end

    def generate_slug
      title.parameterize if respond_to?(:title) && title
    end

    def allow_path_in_slug?
      false
    end

    def slug_presence_required?
      true
    end
  end
end
