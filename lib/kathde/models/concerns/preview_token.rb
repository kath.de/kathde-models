# frozen_string_literal: true

module Kathde::Models::Concerns
  module PreviewToken
    extend ActiveSupport::Concern

    PREVIEW_TOKEN_LENGTH = 16

    included do
      def preview_token
        super || generate_preview_token
      end

      def generate_preview_token
        update_attribute(:preview_token, SecureRandom.urlsafe_base64(PREVIEW_TOKEN_LENGTH))
        preview_token
      end
    end

    class_methods do
      def drafts_with_token(preview_token)
        draft.payload_where(preview_token: preview_token)
      end
    end
  end
end
