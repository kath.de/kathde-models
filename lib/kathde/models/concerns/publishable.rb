# frozen_string_literal: true

module Kathde::Models::Concerns
  module Publishable
    extend ActiveSupport::Concern

    included do
      enum status: { draft: 'draft', published: 'published', deleted: 'deleted', hidden: 'hidden' }

      attribute :status, :enum, default: :draft

      validates :published_at, presence: true, if: :published?

      default_scope { where.not(status: :deleted) }

      scope :unpublished, -> { where.not(status: :published) }

      scope :recent, -> { published.order(published_at: :desc) }
    end

    def publish
      # Reduce to minute precision because submissions through PublishedAtField would trim seconds
      # in a later update.
      self.published_at ||= Time.zone.now.at_beginning_of_minute
      self.status = :published
    end

    def publish!
      if !published? && publish
        if self.respond_to? :paper_trail_event=
          self.paper_trail_event = 'publish'
        end
        save
      end
    end

    def unpublish
      # self.published_at = nil
      self.status = :draft
    end

    def unpublished?
      !published?
    end

    def unpublish!
      if published? && unpublish
        if self.respond_to? :paper_trail_event=
          self.paper_trail_event = 'unpublish'
        end
        save
      end
    end

    def slug_presence_required?
      published?
    end
  end
end
