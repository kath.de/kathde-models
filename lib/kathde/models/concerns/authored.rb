# frozen_string_literal: true

module Kathde::Models::Concerns
  module Authored
    extend ActiveSupport::Concern

    included do
      scope :by_author, lambda { |author|
        if author.is_a? Integer
          where('? = ANY("author_ids")', author)
        else
          by_author_ids(Author.default_scoped.by_term(author))
        end
      }

      scope :by_author_ids, lambda { |author_ids|
        author_sql = if author_ids.respond_to? :to_sql
                      # {}"ARRAY(#{author_ids.select(:id).to_sql})"
                      ids = Arel::Nodes::SqlLiteral.new(author_ids.select(:id).to_sql)
                      Arel::Nodes::NamedFunction.new('ARRAY', [ids])
                    else
                      Arel::Nodes::SqlLiteral.new("(ARRAY[#{author_ids.join(',')}])")
                    end

        # WORKAROUND: author_ids @> ARRAY(...) does not work with empty SELECT result, in that case
        # it ignorse this where condition
        # alternative: author_ids @> (ARRAY(...) || -1)
        where(Arel::Nodes::InfixOperation.new('&&',
                                              Arel::Nodes::SqlLiteral.new('ARRAY[author_ids]::integer[]'),
                                              author_sql))
      }

      before_save do
        self.author_ids = author_ids.compact
      end
    end

    def authors=(authors)
      authors = Array(authors || []).compact
      @authors = authors
      self.author_ids = authors.map(&:id)
    end

    alias author= authors=

    def authors
      if author_ids.any?
        @authors ||= Author.find(author_ids)
      else
        []
      end
    end

    def author
      authors.first
    end
  end
end
