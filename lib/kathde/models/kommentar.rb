# frozen_string_literal: true

require 'kathde/models/entry'

class Kommentar < Entry
  include Kathde::Models::Concerns::PreviewToken
  include Kathde::Models::Concerns::Covered

  WORDS_PER_MINUTE = 250

  PAYLOAD_ATTRIBUTES = {
    title: :string,
    body: :text,
    teaser_text: :text,
    vgwort_counter: :string,
    tags: [:string, array: true],
    preview_token: :string,
  }.freeze

  jsonb_accessor(
    :payload,
    PAYLOAD_ATTRIBUTES,
  )

  validates :type, presence: true
  validates :title, :body, :author_ids, presence: true, if: :published?

  def self.any_search_fields
    %i[title body teaser_text]
  end

  def date_slug
    if published?
      I18n.localize(published_at, format: '%Y-%m-%d')
    else
      I18n.localize(Time.now, format: '%Y-%m-%d')
    end
  end

  def self.scrub_slug(slug)
    # reduce extended slug to date-free slug. Published date may change if a post is republished.
    slug.gsub(/\A\d{4}-\d{2}-\d{2}-/, '')
  end

  def generate_slug
    [date_slug, title.parameterize].join('-') if title
  end

  def text_length_in_minutes
    body.split(' ').size / WORDS_PER_MINUTE
  end
end
