# frozen_string_literal: true

require 'kathde/models/entry'
require 'addressable/uri'

class Publisher < Entry
  PAYLOAD_ATTRIBUTES = {
    title: :string,
    long_title: :string,
  }.freeze

  jsonb_accessor(
    :payload,
    PAYLOAD_ATTRIBUTES,
  )

  alias_attribute :domain, :slug

  validates :type, presence: true
  validates :domain, :title, presence: true

  validates :domain, format: { with: /\A([a-z\-\d]{1,63}\.)+[a-z]{2,}(\/.*)?\z/,
                               message: 'Nur Domain im Format [name].[endung] erlaubt.', }

  def self.by_url(urls)
    if urls.is_a?(String)
      return by_url([urls])[normalize_url(urls)]
    end

    urls = urls.map { |url| normalize_url(url) }.compact
    return Hash.new unless urls && !urls.empty?

    urls_string = urls.map {|url| connection.quote(url) }.join(", ")
    query = <<-SQL
      WITH publishers as (
        SELECT *
        FROM entries
        WHERE type = 'Publisher' AND status != 'deleted'
      )
      SELECT url, publishers.*
      FROM
        (
          SELECT url, max(slug) over (partition by url) as max_slug
          FROM
            publishers,
            unnest(ARRAY[#{urls_string}]) as url
          WHERE url like slug || '%'
        ) AS paths
      JOIN publishers ON (slug = max_slug::varchar);
      SQL

    connection.select_all(query).inject({}) do |publishers, row|
      publishers[row["url"]] = instantiate(row)
      publishers
    end
  end

  scope :recently_created, -> { where('DATE(created_at) = ?', Time.zone.today) }

  def self.normalize_url(url)
    return nil unless url
    uri = Addressable::URI.parse(url)
    return url.downcase.gsub(/\A(www\.)/, '') unless uri.absolute?

    normalized = uri.normalized_host.gsub(/\A(www\.)/, '')

    path = uri.normalized_path
    normalized << path unless path == '/'

    normalized
  rescue URI::InvalidURIError
    nil
  end

  def self.normalize_domain(url)
    return nil unless url
    uri = Addressable::URI.parse(url)
    return url.downcase.gsub(/\A(www\.)/, '') unless uri.absolute?

    uri.normalized_host.gsub(/\A(www\.)/, '')
  rescue URI::InvalidURIError
    nil
  end

  def self.normalize_title(url)
    return nil unless url
    domain = Addressable::URI.parse(url).host || url.split(/\//).first
    domain.gsub(/\Awww\./, '').downcase.capitalize
  rescue URI::InvalidURIError
    nil
  end

  def self.[](url)
    by_url(url) || from_url(url)
  end

  def self.from_url(url)
    url = normalize_url(url)
    title = normalize_title(url)

    new(slug: url, title: title)
  end

  def ==(other)
    other.instance_of?(self.class) && (
      self.title == other.title &&
      self.long_title == other.long_title &&
      self.domain == other.domain
      )
  end

  # def self.cache
  #  @cache

  def allow_path_in_slug?
    true
  end
end
