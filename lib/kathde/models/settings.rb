# frozen_string_literal: true

require 'kathde/models/entry'

class Settings < Entry
  validates :type, presence: true
  alias_attribute :settings, :payload

  def self.[](slug)
    by_slug(slug.to_s).first
  end

  delegate :[], :key, :length, :size, :to_hash, :values, :has_key?, :keys, :fetch,
           :each, :each_key, :each_pair, :each_value, :dig,
           to: :settings

  def key?(name)
    payload.key?(name) unless payload.nil?
  end

  def method_missing(method, *args, &block)
    if key? method.to_s
      payload[method.to_s]
    else
      super
    end
  end

  def respond_to_missing?(method, *)
    payload.key?(method.to_s) || super
  end
end
