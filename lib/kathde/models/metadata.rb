# frozen_string_literal: true

require 'kathde/models/validator/url'

class Metadata
  include ActiveModel::Model
  include ActiveModel::Validations

  attr_accessor :title, :subtitle, :published_at,
                :url, :original_url, :provider,
                :authors, :publisher, :tags

  validates :url, url: true

  validates :url, :title, presence: true

  # attribute :title, :string
  # attribute :subtitle, :string
  # attribute :date, :datetime, default: -> { Time.zone.now }
  # attribute :url, :string
  # attribute :original_url, :string
  # attribute :provider
  # attribute :tags, :string, array: true
end
