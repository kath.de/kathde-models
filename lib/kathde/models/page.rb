# frozen_string_literal: true

require 'kathde/models/entry'

class Page < Entry
  PAYLOAD_ATTRIBUTES = {
    title: :string,
    long_title: :string,
    subtitle: :string,
    body: :text,
  }.freeze

  jsonb_accessor(
    :payload,
    PAYLOAD_ATTRIBUTES,
  )

  validates :type, presence: true
  validates :title, :body, presence: true, if: :published?

  def allow_path_in_slug?
    true
  end

  def self.any_search_fields
    %i[title body]
  end
end
