# frozen_string_literal: true

require 'kathde/models/tagesreport/report_entry'

class Tagesreport::ReportEntry
  include ActiveModel::Model
  attr_accessor :titel, :untertitel, :url, :tagesreport, :section, :related
  attr_writer :date, :publisher, :source

  def initialize(data = {}, section: nil, tagesreport: nil, publisher: nil)
    self.titel = data['titel']
    self.untertitel = data['untertitel']
    self.url = data['url']
    self.publisher = publisher
    self.date = data['datum']
    self.source = data['quelle']

    self.tagesreport = tagesreport
    self.section = section
    self.related = []
  end

  def source
    source = @source
    if source.nil?
      source = publisher&.title
    end
    source = false if source == ""
    source
  end

  def date
    date = @date
    if date.nil?
      date = tagesreport&.date
    end
    date = false if date == ""
    date
  end

  def publisher
    @publisher || Publisher.from_url(url)
  end

  def ==(other)
    other.instance_of?(self.class) && (
      self.titel == other.titel &&
      self.untertitel == other.untertitel &&
      self.url == other.url &&
      self.date == other.date &&
      self.tagesreport == other.tagesreport &&
      self.section == other.section &&
      self.related == other.related &&
      self.source == other.source
      )
  end

  def to_s
    "#{titel} (#{datum}) <#{url}>"
  end

  def self.preload_publishers(data_entries)
    urls = collect_urls(data_entries)
    Publisher.by_url(urls)
  end

  def self.collect_urls(data_entries)
    urls = []
    data_entries.each do |entry|
      urls << entry['url']
      if außerdem = entry['außerdem']
        urls.concat(außerdem.map { |e| e['url'] })
      end
    end
    urls
  end

  def self.new(data = {}, section: nil, tagesreport: nil, publisher: nil, preloaded_publishers: nil)
    if preloaded_publishers
      url = Publisher.normalize_url(data['url'])
      publisher = preloaded_publishers[url] if url
    end

    super(data, publisher: publisher).tap do |entry|
      entry.section = section
      entry.tagesreport = tagesreport

      if related = data['außerdem']
        entry.related = related.map do |sub_data|
          new sub_data,
            section: section,
            tagesreport: tagesreport,
            preloaded_publishers: preloaded_publishers
        end
      end
    end
  end
end
