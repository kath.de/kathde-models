# frozen_string_literal: true

ActiveRecord::Type.register(:value, ActiveRecord::Type::Value)

module Payload
  extend ActiveSupport::Concern

  PAYLOAD_ATTRIBUTES = {}.freeze
  PAYLOAD_FIELD = :payload

  def payload_attributes
    self.class::PAYLOAD_ATTRIBUTES
  end

  def payload_attribute_names
    payload_attributes.keys
  end

  included do
    attribute PAYLOAD_FIELD, :jsonb, default: {}
  end

  class_methods do # rubocop:disable Metrics/BlockLength
    def payload_field
      PAYLOAD_FIELD
    end

    def payload_column
      arel_table[payload_field]
    end

    def payload_attribute?(attribute)
      payload_attributes.keys.include?(attribute.try(:to_sym))
    end

    def payload_attributes
      self::PAYLOAD_ATTRIBUTES
    end
  end
end
