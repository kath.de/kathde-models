# frozen_string_literal: true

require 'kathde/models/entry'

class Wochenrueckblick < Entry
  include Kathde::Models::Concerns::PreviewToken
  include Kathde::Models::Concerns::Covered

  PAYLOAD_ATTRIBUTES = {
    title: :string,
    body: :text,
    teaser_text: :text,
    preview_token: :string,
  }.freeze

  jsonb_accessor(
    :payload,
    PAYLOAD_ATTRIBUTES,
  )

  validates :type, presence: true
  validates :title, :slug, presence: true
  validates :title, :body, :authors, presence: true, if: :published?

  def self.any_search_fields
    %i[title body teaser_text]
  end
end
