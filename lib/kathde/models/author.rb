# frozen_string_literal: true

require 'kathde/models/entry'
require 'kathde/models/validator/url'

class Author < Entry
  include Kathde::Models::Concerns::Covered

  PAYLOAD_ATTRIBUTES = {
    display_name: :string,
    positions: [:string, array: true],
    profile_image: :string,
    initials: :string,
    email: :string,
    short_bio: :text,
    body: :text,
  }.freeze

  jsonb_accessor(
    :payload,
    PAYLOAD_ATTRIBUTES,
  )

  validates :type, presence: true
  validates :display_name, presence: true
  validates :email, format: { with: /\A[^@\s]+@[^@\s]+\.[a-z]{2,}\z/i }
  validates :initials, length: { maximum: 5 }

  scope :by_term, lambda { |terms|
    return by_any(terms, %i[slug display_name], set_operator: :disjunction)
  }

  def self.list_redaktion
    all.payload_order(display_name: :asc).published
  end

  def entries
    Entry.published.by_author(id)
  end

  def recent_works
    entries.order(published_at: :desc)
  end

  def recent_kommentare
    Kommentar.published.by_author(id).order(published_at: :desc)
  end

  def generate_slug
    display_name&.parameterize
  end

  def as_json(_options = {})
    super(only: %i[display_name slug id],
          methods: [:slug])
  end
end
