# frozen_string_literal: true

require 'kathde/models/entry'

class Tagesreport < Entry
  include Kathde::Models::Concerns::PreviewToken
  include Kathde::Models::Concerns::Covered

  DEFAULT_SECTIONS = [
    'Themen des Tages',
    'Service und Spiritualität',
    'Bistümer und Werke',
    'Pressespiegel',
  ].freeze

  ALLOWED_DESERIALIZATION_CLASSES = [Date].freeze

  PROFILE_JSON_SCHEMA = File.join(File.dirname(__FILE__), 'tagesreport', 'tagesreport-entries.json_schema').to_s

  PAYLOAD_ATTRIBUTES = {
    display_title: :string,
    aufmacher_title: :string,
    aufmacher_body: :text,
    date: :date,
    entries: :value,
    preview_token: :string,
  }.freeze

  jsonb_accessor(
    :payload,
    PAYLOAD_ATTRIBUTES,
  )

  validates :type, presence: true
  validates :date, :slug, presence: true

  validates :aufmacher_title, :aufmacher_body, :authors, :date, :entries,
            presence: true, if: :published?

  scope :for_date, ->(year, month, day) { where('slug = ?', [year, month, day].join('-')) }

  if defined?(JsonValidator)
    # This validator is only used in the CMS backend
    validates :entries_hash, presence: true, json: { message: ->(errors) { errors }, schema: PROFILE_JSON_SCHEMA }, if: :published?
  end

  def entries_list
    sections.values.flatten.compact
  end

  def sections(preloaded_publishers = nil)
    @sections ||= begin
      preloaded_publishers ||= ReportEntry.preload_publishers(entries_hash.values.flatten.compact)
      entries_hash.map do |section, items|
        next [section, []] if items.nil?

        [
          section,
          items.map do |data|
            ReportEntry.new data,
                            section: section,
                            tagesreport: self,
                            preloaded_publishers: preloaded_publishers
          end,
        ]
      end.to_h
    end
  end

  # Entries are stored as YAML string in database to keep original formatting and comments
  def entries_yaml
    entries
  end

  def entries_hash
    self.class.parse_entries_hash(entries_yaml) if entries_yaml.present?
  rescue Psych::SyntaxError => e
    errors.add(:entries, :invalid_yaml, error: e.message)
    {}
  end

  def yaml_error
    @yaml_error
  end

  def self.parse_entries_hash(yaml)
    YAML.safe_load(yaml, ALLOWED_DESERIALIZATION_CLASSES, [], true)
  end

  def entries_hash=(hash)
    self.entries = hash.deep_stringify_keys.to_yaml
  end

  def section_titles
    entries_hash.keys.reject { |key| entries_hash[key].empty? }
  end

  def title
    "Tagesreport #{I18n.localize(date, format: '%-d. %B %Y')}"
  end

  def default_display_title
    "kath.de-Nachrichtenüberblick für #{I18n.localize(date || Time.zone.now, format: '%A, %-d. %B %Y')}"
  end

  def to_s
    "#{title} (#{entries.size} entries)"
  end

  def day_of_week
    date.strftime('%A').downcase
  end

  def self.newest
    recent.first
  end

  def self.default_entries_content(day_of_week = nil)
    if day_of_week
      templates = Settings[:tagesreport_templates]
      found = templates.dig(day_of_week, 'vorlage') if templates
      return found if found
    end

    DEFAULT_SECTIONS.map { |section| %("#{section}":\n) }.join("\n")
  end

  def hinweistext
    templates = Settings[:tagesreport_templates]
    return '' unless templates

    [
      templates.dig('everyday', 'hinweistext'),
      templates.dig(day_of_week, 'hinweistext'),
    ].join("\n\n")
  end

  def generate_slug
    date.to_s if date
  end
end

require 'kathde/models/tagesreport/report_entry'
