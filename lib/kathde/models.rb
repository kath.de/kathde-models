# frozen_string_literal: true
require 'logger'

module Kathde
  module Models
    # Your code goes here...
    def self.logger
      if defined?(Mainsite)
        Mainsite::App.logger
      elsif defined?(Rails)
        Rails.logger
      elsif
        local_logger
        local_logger
      end
    end

    def self.local_logger
      @@logger ||= Logger.new(STDOUT)
    end
  end
end

require 'kathde/models/version'
require 'kathde/models/author'
require 'kathde/models/entry'
require 'kathde/models/image'
require 'kathde/models/kommentar'
require 'kathde/models/metadata'
require 'kathde/models/page'
require 'kathde/models/publisher'
require 'kathde/models/settings'
require 'kathde/models/tagesreport'
require 'kathde/models/wochenrueckblick'
