# coding: utf-8
# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'kathde/models/version'

Gem::Specification.new do |spec|
  spec.name          = 'kathde-models'
  spec.version       = Kathde::Models::VERSION
  spec.authors       = ['Johannes Müller']
  spec.email         = ['johannes.mueller@kath.de']

  spec.summary       = 'Unified Models for Kath.de services.'
  spec.description   = ''
  spec.homepage      = 'https://kath.de'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = ''
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rspec', '~> 3.8'
  spec.add_development_dependency 'database_cleaner'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'standalone_migrations', '< 7'
  spec.add_development_dependency 'factory_bot'
  spec.add_development_dependency 'activerecord_json_validator'
  spec.add_development_dependency 'rails', '< 7'
  spec.add_development_dependency 'rspec-rails'

  spec.add_dependency 'activerecord', '>= 3.1', '< 7'
  spec.add_dependency 'activestorage', '< 7'
  spec.add_dependency 'active_storage_validations'
  spec.add_dependency 'jsonb_accessor', '~> 1.0'
  spec.add_dependency 'addressable', '~> 2.5'
end
